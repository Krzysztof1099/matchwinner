package com.demo.springboot.domain;


import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class MatchDto {
    private String match;

    public String getMatch() {
        return match;
    }
}

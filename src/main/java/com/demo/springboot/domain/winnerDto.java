package com.demo.springboot.domain;


import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;


@ToString
@NoArgsConstructor
@Component
public class winnerDto {
    private String team;
    private String goals;

    public winnerDto(String x, String y){
        team=x;
        goals=y;
    }

    public String getTeam() {
        return team;
    }

    public String getGoals() {
        return goals;
    }
}
